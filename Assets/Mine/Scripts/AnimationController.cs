﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
    [SerializeField]
    float jumpForce;
    Animator anim;
    Rigidbody rb;
    bool grounded, attacking;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {


        anim.SetBool("Groudon", grounded);
        anim.SetBool("Jonson", attacking);


        if (Input.GetKeyDown(KeyCode.Z)){
            anim.SetTrigger("kick");
            attacking = true;
	    }
        if(Input.GetKeyDown(KeyCode.X)){
            anim.SetTrigger("punch");
            attacking = true;
        }
            if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f){

            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }   
        if (grounded) { 
            if (Input.GetKeyDown(KeyCode.Space)) {
                anim.SetTrigger("Saltaseishon");

            }
        }    
    }
     void OnCollisionStay(Collision coll)
    {
        if (coll.gameObject.tag == "Groudon")
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }
    private void OnCollisionExit(Collision drt)
    {
        if (drt.gameObject.tag == "Groudon")
        {
            grounded = false;
        }
    }

    void ActualJump() {
        print("jumper");
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
             }


}

