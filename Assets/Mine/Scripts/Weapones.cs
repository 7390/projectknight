﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapones : MonoBehaviour {
    [Header("Axes")]
    [SerializeField] GameObject backAxe;
    [SerializeField] GameObject rPalmAxe;
    [SerializeField] GameObject lPalmAxe;
    [Header("Swords")]
    [SerializeField] GameObject backSword;
    [SerializeField] GameObject lPalmSword;
    [Header("Shields")]
    [SerializeField] GameObject backShield;
    [SerializeField] GameObject rPalmShield;
    [Header("Bows")]
    [SerializeField] GameObject backBow;
    [SerializeField] GameObject rPalmBow;
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }




    void BackAxe()
    {
        backAxe.SetActive(true);
        rPalmAxe.SetActive(false);
        lPalmAxe.SetActive(false);
    }
    void RAxe()
    {
        backAxe.SetActive(false);
        rPalmAxe.SetActive(true);
        lPalmAxe.SetActive(false);
    }
    void LAxe()
    {
        backAxe.SetActive(false);
        rPalmAxe.SetActive(false);
        lPalmAxe.SetActive(true);
    }
    void BackSword()
    {
        backSword.SetActive(true);
        lPalmSword.SetActive(false);
    }
    void LSword()
    {
        backSword.SetActive(false);
        lPalmSword.SetActive(true);
    }
    void BackShield()
    {
        backShield.SetActive(true);
        rPalmShield.SetActive(false);
    }
    void RShield()
    {
        backShield.SetActive(false);
        rPalmShield.SetActive(true);
    }
    void BackBow()
    {
        backBow.SetActive(true);
        rPalmBow.SetActive(false);
    }
    void RBow()
    {
        backBow.SetActive(false);
        rPalmBow.SetActive(true);
    }

    

}
