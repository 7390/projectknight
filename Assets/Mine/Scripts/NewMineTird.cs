﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
[RequireComponent(typeof(MineImputer))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class NewMineTird : MonoBehaviour
{

    [System.Serializable]
    public class MouseInput
    {
        public Vector2 Damping = new Vector2(5, 0), Sensitivity = new Vector2(5, 0);
    }
    [SerializeField] Damager d_Sword, d_Axe;
    [SerializeField] Bow b_Bow;
    Animator anim;
    MineImputer mineinputer;
    IthemsCounter items;
    Rigidbody rb;
    MineLifer lifer;
    [SerializeField]
    float speed = 2f, runSpeed = 5f, groundDistance = 0.5f, fuerzaDeSalto = 50f;
    [SerializeField]
    MouseInput mouseControl;
    Transform camT;

    float tiempoDeSalto = 0.5f;
    bool running, attaking, grounded, jump, alive;

    public bool axe, bow, shield, sword, magic;
    bool axeAble, bowAble, shielAble, swordAble, magicAble;
    int currentAbilitie;

    private void Start()
    {
        items = GetComponent<IthemsCounter>();
        anim = GetComponent<Animator>();
        camT = Camera.main.transform;
        mineinputer = GetComponent<MineImputer>();
        rb = GetComponent<Rigidbody>();
        lifer = GetComponent<MineLifer>();
        magic = true;
        alive = true;
    }

    Vector2 mouseImput;
    private void Update()
    {
        if (alive)
        {
            if(lifer.currentLife <= 0f)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetTrigger("Death");
                alive = false;
            }
            Grounder();
            Animate();
            Move();
            CheckForRun();
            ChechForJump();
            CheckForAttack();
            Powers();
            CheckForAbilitie();
        }
    }
    public void AnimateThis(string trigger)
    {
        anim.SetTrigger(trigger);
    }
    private void Animate()
    {
        anim.SetFloat("FordWard", running ? mineinputer.movement.y * 2 : mineinputer.movement.y);
        anim.SetFloat("Strafe", mineinputer.movement.x);
        anim.SetBool("Ground", grounded);
        anim.SetBool("Axe", axe);
        anim.SetBool("Bow", bow);
        anim.SetBool("Sword", sword);
        anim.SetBool("Magic", magic);
        anim.SetBool("Attacking", attaking);
        if ((lifer.currentLife > 0) && (!attaking) && (sword || axe || bow))
            anim.SetLayerWeight(1, 1);
    

    }
    private void Move()
    {
            float finalSpeed;
            finalSpeed = running ? runSpeed : speed;
            Vector3 camFord = Vector3.Scale(camT.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 move = (mineinputer.movement.x * camT.right) + (mineinputer.movement.y * camFord);
            Vector3 movement = move.normalized * finalSpeed;
            if (jump)
            {

                tiempoDeSalto -= Time.deltaTime;
                float fuerzaFinalDeSalto = ((tiempoDeSalto * tiempoDeSalto) * fuerzaDeSalto);
                movement.y = fuerzaFinalDeSalto;
                if (tiempoDeSalto < 0)
                    jump = false;
            }
            else
            {
                movement.y = rb.velocity.y;
            }

            rb.velocity = movement;
            mouseImput.x = Mathf.Lerp(mouseImput.x, mineinputer.look.x, 1 / mouseControl.Damping.x);
            transform.Rotate(Vector3.up * mouseImput.x * mouseControl.Sensitivity.x);
    }
    void CheckForRun()
    {
        running = mineinputer.a;
    }
    void CheckForAttack()
    {
        if (!attaking && mineinputer.xD)
        {
            anim.SetLayerWeight(1, 0);
            anim.SetTrigger("Fire1");
            attaking = true;
        }
    }
    private void ChechForJump()
    {
        if (grounded && mineinputer.bD)
        {
            anim.SetTrigger("Jump");
            tiempoDeSalto = 0.5f;
        }
    }
    void Jump()
    {
        jump = true;
    }
    private void Grounder()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundDistance))
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.blue);
            grounded = true;

        }
        else
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.red);
            grounded = false;
            attaking = false;
        }
    }
    void Powers()
    {
        axeAble = items.axe;
        swordAble = items.sword;
        shielAble = items.shield;
        bowAble = items.bow;
        magicAble = items.magicPotions > 0 ? true : false;
    }
    public void AbleToAttack()
    {
        if (sword)
            d_Sword.GetComponent<BoxCollider>().enabled = false;
        if (axe)
            d_Axe.GetComponent<BoxCollider>().enabled = false;
        attaking = false;
    }
    void ActiveWeapon()
    {
        print("Actived");
        if (sword)
            d_Sword.GetComponent<BoxCollider>().enabled = true;
        if (axe)
            d_Axe.GetComponent<BoxCollider>().enabled = true;
        if (bow)
            b_Bow.FireBow();

    }
    private void CheckForAbilities()
    {
        bool[] powers = new bool[4];
        powers[0] = magicAble;
        powers[1] = swordAble;
        powers[2] = axeAble;
        powers[3] = bowAble;
        if (currentAbilitie == powers.Length - 1)
        {
            currentAbilitie = 0;
        }
        else
        {
            for (int a = currentAbilitie +1; a < powers.Length; a++)
            {
                if (powers[a] == true)
                {
                    currentAbilitie = a;
                    return;
                }
                if (a == powers.Length - 1 && powers[a] == false)
                {
                    currentAbilitie = 0;
                }
            }
        }
    }
    void CheckForAbilitie()
    {
        if (mineinputer.yD)
        {
            CheckForAbilities();
            switch (currentAbilitie)
            {
                case 0:
                    magic = true; ;
                    sword = false;
                    bow = false;
                    axe = false;
                    break;
                case 1:
                    magic = false;
                    sword = true;
                    bow = false;
                    axe = false;
                    break;
                case 2:
                    magic = false;
                    sword = false;
                    bow = false;
                    axe = true;
                    break;
                case 3:
                    magic = false;
                    sword = false;
                    bow = true;
                    axe = false;
                    break;
            }
        }
    }
}