﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTird : MonoBehaviour {
    [SerializeField] Vector3 cameraOffset;
    [SerializeField] float damping;

    Transform lookTarject;
    NewMineTird player;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<NewMineTird>();
        lookTarject = player.transform.Find("Tarject");
        if(lookTarject == null)
        {
            lookTarject = player.transform;
        }
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 tarjectPosition = lookTarject.position + player.transform.forward * cameraOffset.z +
            player.transform.up * cameraOffset.y +
            player.transform.right * cameraOffset.x;
        Quaternion tarjectRotation = Quaternion.LookRotation(lookTarject.position - tarjectPosition, Vector3.up);
        transform.position = Vector3.Lerp(transform.position, tarjectPosition, damping * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, tarjectRotation, damping * Time.deltaTime); 
	}
}
