﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineImputer : MonoBehaviour {

    public bool start, select, x, y, a, b, startD, selectD, xD, yD, aD, bD, startU, selectU, xU, yU, aU, bU;
    public Vector2 movement, look;
    private void Update()
    {
        start = Input.GetButton("Submit");
        startD = Input.GetButtonDown("Submit");
        startU = Input.GetButtonUp("Submit");
        select = Input.GetButton("Cancel");
        selectD = Input.GetButtonDown("Cancel");
        selectU = Input.GetButtonUp("Cancel");
        x = Input.GetButton("Fire1");
        xD = Input.GetButtonDown("Fire1");
        xU = Input.GetButtonUp("Fire1");
        y = Input.GetButton("Fire2");
        yD = Input.GetButtonDown("Fire2");
        yU = Input.GetButtonUp("Fire2");
        a = Input.GetButton("Fire3");
        aD = Input.GetButtonDown("Fire3");
        aU = Input.GetButtonUp("Fire3");
        b = Input.GetButton("Jump");
        bD = Input.GetButtonDown("Jump");
        bU = Input.GetButtonUp("Jump");
        movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        look = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
    }
}
