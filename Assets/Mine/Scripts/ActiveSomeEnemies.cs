﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSomeEnemies : MonoBehaviour {
    [SerializeField] Enemie1[] enemies;
    [SerializeField] bool canQuit;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<NewMineTird>() != null)
        {
            foreach(Enemie1 t in enemies)
            {
                t.Agent(other.transform);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (canQuit)
        {
            if (other.GetComponent<NewMineTird>() != null)
            {
                foreach (Enemie1 t in enemies)
                {
                    t.DisAgent();
                }
            }
        }

    }
}
