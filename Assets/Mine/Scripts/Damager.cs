﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
public class Damager : MonoBehaviour {
    [SerializeField] int potencia = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        Destroyable terject = other.GetComponent<Destroyable>();
        if(terject != null)
        {
            terject.SartDestroy(potencia);
            return;
        }
        MineLifer lifer = other.GetComponent<MineLifer>();
        MineLifer liferp = other.gameObject.GetComponentInParent<MineLifer>();

        if (lifer != null)
        {
            lifer.Damage(potencia);
            return;
        }
        if (liferp != null)
        {
            liferp.Damage(1);
            return;
        }

    }
}
